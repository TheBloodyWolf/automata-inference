#include <iostream>
#include <fstream>
#include <random>
#include <chrono>
#include <string.h>

#include <base/sample.h>
#include <base/solution.h>

#include <eval/basicEval.h>
#include <eval/smartEval.h>
#include <eval/basicBiobjEval.h>
#include <eval/smartBiobjEval.h>

#include <ILS/ILS.hpp>


using namespace std;


typedef pair<double, unsigned> Fitness2;

/*
	Main
*/
int main(int argc, char ** argv) {
	chrono::milliseconds start = chrono::duration_cast< chrono::milliseconds >(
		chrono::system_clock::now().time_since_epoch()
	);
	if (argc < 2) {
		cout << " expected arguments : -train ( sample filename ) ( nStates ) ( random seed ) ( nEval ) or -test ( sample filename ) ( solution filename )" << endl;
		return -1;
	}
	// minimal example of sample
	Sample sample(argv[2]);

	if (strcmp(argv[1], "-test") == 0) {
		if (argc != 4) {
			cout << "expected arguments : -test (sample filename) ( random seed )" << endl;
			return -1;
		}

		// Another candidate solution, read from file
		Solution<double> xprime;

		// read from a file this solution
		fstream filein("solution.json");
		if (!filein) {
			std::cerr << "Impossible to open " << argv[3] << std::endl;
		}
		filein >> xprime;

		filein.close();

        std::mt19937 gen(stoi(argv[3]));
		SmartEval eval( gen, sample);
		eval(xprime);
        chrono::milliseconds end = chrono::duration_cast< chrono::milliseconds >(
                chrono::system_clock::now().time_since_epoch()
        );
        cout << xprime.fitness() << " ";
        cout << end.count() - start.count() << std::endl;
	}
	else if (strcmp(argv[1], "-train") == 0) {
		if (argc < 4) {
			cout << "expected arguments : -train ( sample filename ) ( nStates ) ( random seed ) ( nEval ) " << endl;
			return -1;
		}
		

		std::mt19937 gen(stoi(argv[4]));

		SmartEval eval( gen, sample );
		ILS hc(eval, stoi(argv[5] ) , gen);
		Solution<double> sol( stoi( argv[3] ) , 2);
		hc.run(sol);

		ofstream ofs( "solution.json" );
		ofs << sol;
		ofs.close();
		chrono::milliseconds end = chrono::duration_cast< chrono::milliseconds >(
			chrono::system_clock::now().time_since_epoch()
			);
		cout << sol.fitness() << " ";
		cout << end.count() - start.count() << std::endl;
	}
	else cout << " expected arguments : -train ( sample filename ) ( nStates ) ( random seed ) ( nEval ) or -test ( sample filename ) ( solution filename )" << endl;
	return 0;
}