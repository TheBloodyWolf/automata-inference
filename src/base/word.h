/*
  word.h

 Author: 
  Sebastien Verel, 
  Univ. du Littoral Côte d'Opale, France.
 
 Date:
   2019/04/08 : Version 0
*/

#ifndef _word_h
#define _word_h

#include "rapidjson/document.h"

#include <string>
#include <iostream>
#include <vector>

/*
    Word : 
        vector of digit, and accept = true when the word belongs to the langage
*/
class Word {
public:
    Word() {
        accept = false;
    }

    Word(const Word & _w) {
        s = _w.s;
        accept = _w.accept;
    }

    Word(std::vector<unsigned> & _s) {
        // copy
        s = _s;

        accept = false;
    }

    Word(std::vector<unsigned> & _s, bool _accept) {
        // copy
        s = _s;
        
        accept = _accept;
    }


    // string of the word
    std::vector<unsigned> s;

    // accept is true when the word is in the language
    bool accept;

    /*
        Output
    */
    virtual void printOn(std::ostream& _os) const {
        _os << "{" ;

        _os << "\"s\":[" ;
        if (s.size() > 0) {
            _os << s[0] ;
            for(unsigned i = 1; i < s.size(); i++)
                _os << "," << s[i] ;
        }
        _os << "]," ;

        _os << "\"a\":" ;
        if (accept)
            _os << "true";
        else
            _os << "false";

        _os << "}" ;
    }

    /*
        Input
    */
    virtual void readFrom(const rapidjson::Value& document) {
        if (document.IsObject()) {
            if (document.HasMember("s")) {
                    for(const auto& c : document["s"].GetArray()) {
                        s.push_back(c.GetInt());
                    }
                    accept = document["a"].GetBool();
            }  else
                std::cerr << "Error: string s not found." << std::endl;

        } else {
            std::cerr << "word::readFom: Impossible to parse the json stream." << std::endl;            
        }
    }

    /*
        Input
    */
    virtual void readFrom(std::istream& _is) {
        std::string json((std::istreambuf_iterator<char>(_is)),
                 std::istreambuf_iterator<char>());

        rapidjson::Document document;

        document.Parse(json.c_str());

        if (document.IsObject()) {
            readFrom(document);
        } else {
            std::cerr << "Word::readFrom: Impossible to parse the json istream " << std::endl;            
        }
    }
};

std::ostream & operator<<(std::ostream& _os, const Word& _s) {
    _s.printOn(_os);
    return _os;
}

std::istream & operator >> (std::istream& _is, Word& _w) {
  _w.readFrom(_is);
  return _is;
}


#endif