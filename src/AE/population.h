//
// Created by cbarbie1 on 13/12/2019.
//

#ifndef AUTOMATA_INFERENCE_POPULATION_H
#define AUTOMATA_INFERENCE_POPULATION_H

#include <vector>
#include "../base/solution.h"
#include <random>

class Population : public std::vector<Solution<double>> {
public:
    Population(unsigned int size) : std::vector<Solution<double>>(size){

    }

    Population(unsigned int size , unsigned _nStates, unsigned _alphabetSize , std::mt19937& rand){
        for(unsigned int i=0; i < size; i++){
            Solution<double> sol( _nStates, _alphabetSize);
            sol.randomSolution( rand );
            push_back(sol);
        }
    }
};


#endif //AUTOMATA_INFERENCE_POPULATION_H
