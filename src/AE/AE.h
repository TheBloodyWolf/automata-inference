//
// Created by cbarbie1 on 13/12/2019.
//

#ifndef AUTOMATA_INFERENCE_AE_H
#define AUTOMATA_INFERENCE_AE_H

#include "population.h"
#include <random>
#include <algorithm>
#include <iostream>

class AE {
private:
    int nEvalMax;
    Eval<double>& evalFunc;
    Population parents;
    Population sons;
    std::mt19937& random;

public:
    AE( Eval<double>& evalFunc, int nEvalMax, unsigned int mu, unsigned int lambda, std::mt19937 rand ) :
        evalFunc( evalFunc ), nEvalMax( nEvalMax ),parents( mu ), sons( lambda ), random( rand ){
    }

    void parentalSelection(){
        std::uniform_int_distribution<unsigned > dist( 0, parents.size() - 1 );
        for(unsigned i=0; i<sons.size(); i++){
            sons[i] = parents[ dist(random) ];
        }
    }

    void variations(){
        for(unsigned i=0; i<sons.size(); i++){
            sons[i].mutation( random );
        }
    }

    void evals(){
        for( unsigned int i=0 ; i < sons.size(); i++ ){
            evalFunc( sons[i] );
        }
    }

    void survivorsSelection(){
        std::sort( sons.begin(), sons.end());
        for(unsigned i=0; i < parents.size(); i++ ){
            parents[ i ] = sons[ sons.size() - 1 - i ];
        }
    }

    void run( Population population ){
        parents = population;
        for(unsigned i=0; i<population.size(); i++){
            evalFunc(population[i]);
        }
        for (unsigned int i=0 ; i < nEvalMax; i++ ){
            parentalSelection();
            variations();
            evals();
            i += sons.size();
            survivorsSelection();
        }
    }

};


#endif //AUTOMATA_INFERENCE_AE_H
