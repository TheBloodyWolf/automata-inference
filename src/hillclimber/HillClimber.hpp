#ifndef HILLCLIMBER_HPP
#define HILLCLIMBER_HPP

#include "../eval/eval.h"
#include "../base/solution.h"
#include <random>
#include <vector>
#include <fstream>
#include <iostream>

class HillClimber
{
private:
    int nEvalMax;
    Eval<double>& evalFunc;
	std::mt19937& random;
public:
	
	HillClimber(Eval<double>& evalFunc, int nEvalMax, std::mt19937& r) : evalFunc(evalFunc), nEvalMax(nEvalMax), random(r)
	{

	}

	void run(Solution<double> &s) {
		// Random solution start	
		std::uniform_real_distribution<double> dist(0, 1);
		s.randomSolution( random );
		evalFunc( s );
		Solution<double> bestSol(s);
		std::vector< Solution< double > > neighbors = s.getAllNeighbors();
		Solution<double> bestSolVoisin( s );
		Solution<double> swap;
		for ( int eval = 0; eval < nEvalMax; eval++) {
			for (int voisin = 0; voisin < neighbors.size(); voisin++) {
				int indexRandom = int(dist(random) * (neighbors.size() - voisin)) % (neighbors.size() - voisin);
				s = neighbors[indexRandom];
				evalFunc(s);
				eval++;
				if ( bestSolVoisin.fitness() < s.fitness() ) {
					bestSolVoisin = s;
				}
				swap = neighbors[neighbors.size() - voisin - 1];
				neighbors[neighbors.size() - voisin - 1 ] = s;
				neighbors[indexRandom] = swap;
				if( eval >= nEvalMax )
                    break;
			}
			if ( bestSol.fitness() < bestSolVoisin.fitness() ) {
				bestSol = bestSolVoisin;
				if ( bestSol.fitness() == 1.0 ) {
					s = bestSol;
					return;
				}
			}
			else break;
			neighbors = bestSolVoisin.getAllNeighbors();
		}
		s = bestSol;
	}
};

#endif // HILLCLIMBER_HPP
