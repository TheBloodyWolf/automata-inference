#ifndef HILLCLIMBERBIOBJ_HPP
#define HILLCLIMBERBIOBJ_HPP

#include "../eval/eval.h"
#include <random>
#include <vector>
#include <fstream>
#include <iostream>

class HillClimberBiobj
{
private:
	int nEvalMax;
	Eval< std::pair<double, unsigned>>& evalFunc;
	std::mt19937 random;
public:

	HillClimberBiobj( Eval<std::pair<double, unsigned> >& evalFunc, int nEvalMax, std::mt19937 r ) : evalFunc( evalFunc ), nEvalMax( nEvalMax ), random( r )
	{

	}

	void run( Solution< std::pair<double, unsigned> > &s ) {
		// Random solution start	
		std::uniform_real_distribution<double> dist( 0, 1 );
		s.randomSolution( random );
		evalFunc( s );
		Solution< std::pair<double, unsigned> > bestSol( s );
		std::vector< Solution< std::pair<double, unsigned> > > neighbors = s.getAllNeighbors();
		Solution< std::pair<double, unsigned>> bestSolVoisin( s );
		Solution< std::pair<double, unsigned>> swap;
		for ( int eval = 0; eval < nEvalMax; eval++ ) {
			for ( int voisin = 0; voisin < neighbors.size(); voisin++ ) {
				int indexRandom = int( dist( random ) * ( neighbors.size() - voisin ) ) % ( neighbors.size() - voisin );
				s = neighbors[ indexRandom ];
				evalFunc( s );
				if ( bestSolVoisin.fitness().first < s.fitness().first ){
					bestSolVoisin = s;
				}
				swap = neighbors[ neighbors.size() - voisin - 1 ];
				neighbors[ neighbors.size() - voisin - 1 ] = s;
				neighbors[ indexRandom ] = swap;
			}
			if ( bestSol.fitness().first <= bestSolVoisin.fitness().first ) {
				if ( bestSolVoisin.fitness().first == 1.0 ) {
					if ( bestSolVoisin.fitness().second == bestSolVoisin.nStates ) {
						s = bestSolVoisin;
						return;
					}
					else {
						bestSol = bestSolVoisin;
						Solution< std::pair<double, unsigned>> bestSolVoisin( bestSolVoisin.fitness().second, 2 );
						bestSolVoisin.randomSolution( random );
					}
				}
				else {
					bestSol = bestSolVoisin;
				}
			}
			else bestSolVoisin.randomSolution( random );
			neighbors = bestSolVoisin.getAllNeighbors();
		}
		s = bestSol;
	}
};

#endif // HILLCLIMBERBIOBJ_HPP
