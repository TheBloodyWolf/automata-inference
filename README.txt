Projet Optimisation / recherche opérationnelle
Master 1 informatique, Université du Littoral Côte d'Opale.

Deterministic Finite Automata Inference

see: 
http://www-lisic.univ-littoral.fr/~verel/TEACHING/19-20/RO-M1app/index.html
http://www-lisic.univ-littoral.fr/~verel/TEACHING/19-20/RO-M1/index.html


*** To compile the demo and the main :

cd build
cmake .
make


*** To runthe demo:
./demo

*** To run hill-climber
./main -train ( sample filename ) ( nStates ) ( random seed ) ( nEval )

*** To run test on a sample
./main -test ( sample filename ) ( solution filename )


