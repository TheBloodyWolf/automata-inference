#!/bin/bash

nbEvalList="100000 500000 1000000"
echo "nbEval nbStates precision fitness time" > "performanceTrainHC.csv"
echo "nbEval nbStates precision fitness time" > "performanceTestHC.csv"
echo "nbEval nbStates precision fitness time" > "performanceTrainILS.csv"
echo "nbEval nbStates precision fitness time" > "performanceTestILS.csv"
for n in ${nbEvalList}
do
	echo $n
	( for file in $(find ../instances/*_train-sample.json -type f) ; do
		echo $file
		states=$(echo $file|sed -n "s/.*dfa_\([0-9][0-9]*\)_.*/\1/p" )
		seed=$(echo $file|sed -n "s/.*_\([0-9][0-9]*\)_.*/\1/p")
		precision=$(echo $file|sed -n "s/.*_\(0.[0-9][0-9]*\)_.*/\1/p")
		testfile=$(echo $file|sed -n "s/train/test/p")
		echo $n $states $precision $(./mainHC -train ${file} ${states} ${seed} ${n}) >> "performanceTrainHC.csv"
		echo $n $states $precision $(./mainHC -test ${testfile} ${seed}) >> "performanceTestHC.csv"
		echo $n $states $precision $(./mainILS -train ${file} ${states} ${seed} ${n}) >> "performanceTrainILS.csv"
		echo $n $states $precision $(./mainILS -test ${testfile} ${seed}) >> "performanceTestILS.csv"
	  done )
done
