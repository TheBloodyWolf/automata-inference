#!/bin/bash

nbEvalList="1000000"
echo "nbEval nbStates precision id idrun fitness time" > "performanceTrainILS.csv"
echo "nbEval nbStates precision id idrun fitness time" > "performanceTestILS.csv"


for i in {0..30}
do
  (for n in ${nbEvalList}
  do
    echo $n
    ( for file in $(find ../instances/*_train-sample.json -type f) ; do
      echo $file
      states=$(echo $file|sed -n "s/.*dfa_\([0-9][0-9]*\)_.*/\1/p" )
      seed=$(echo $file|sed -n "s/.*_\([0-9][0-9]*\)_.*/\1/p")
      precision=$(echo $file|sed -n "s/.*_\(0.[0-9][0-9]*\)_.*/\1/p")
      testfile=$(echo $file|sed -n "s/train/test/p")
      echo $n $states $precision $seed $i $(./mainILS -train ${file} ${states} $i ${n}) >> "performanceTrainILS.csv"
      echo $n $states $precision $seed $i $(./mainILS -test ${testfile} $i) >> "performanceTestILS.csv"
      done )
  done)
done
